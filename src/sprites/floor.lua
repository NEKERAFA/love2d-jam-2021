local class = require "hump.class"
local push = require "push"
local PM = require "playmat"

local floor = class {
    init = function(self)
        self.camera = PM.newCamera(push:getWidth(), push:getHeight(), 0, 0, -math.pi / 2, push:getHeight() / 2, 0.627)
        self.image = love.graphics.newImage("assets/sprites/floor.png")
    end,

    draw = function(self, pos)
        PM.drawPlane(self.camera, self.image, - math.max(math.min(pos / 2, 944), 80), -50)
    end
}

return floor