local class = require "hump.class"

local tree = class {
    init = function(self, initialPos)
        self.initialPos = initialPos
        self.image = love.graphics.newImage("assets/sprites/trees.png")
        self.sprite = love.graphics.newQuad(32 * love.math.random(0, 3), 0, 32, 48, self.image:getWidth(), self.image:getHeight())
        self.flipped = love.math.random(2) == 2
    end,

    draw = function(self, pos)
        local offset = (self.flipped and 32) or 0
        love.graphics.draw(self.image, self.sprite, self.initialPos * 2 - math.max(math.min(pos * 2, 3776), 320) - offset, 144, 0, (self.flipped and 2) or -2, 2)
    end
}

return tree