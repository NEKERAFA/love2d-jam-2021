local class = require "hump.class"

local house3 = class {
    init = function(self)
        self.image = love.graphics.newImage("assets/sprites/house3.png")
    end,

    draw = function(self, pos)
        love.graphics.draw(self.image, 595 - math.min(pos, 1888))
    end
}

return house3