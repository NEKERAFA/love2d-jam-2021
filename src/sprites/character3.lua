local class = require "hump.class"
local anim8 = require "anim8"

local character3 = class {
    init = function(self)
        self.image = love.graphics.newImage("assets/sprites/character3.png")
        self.type = 1

        local grid = anim8.newGrid(10, 23, self.image:getWidth(), self.image:getHeight())
        self.walk = anim8.newAnimation(grid("1-2", 1, 1, 1, 3, 1), 0.15)
        self.walk:flipH()
        self.stay = love.graphics.newQuad(30, 0, 12, 23, self.image:getWidth(), self.image:getHeight())
    end,

    update = function(self, dt)
        if self.type == 1 then
            self.walk:update(dt)
        end
    end,

    draw = function(self, x, y)
        if self.type == 1 then
            self.walk:draw(self.image, x, y, 0, 2, 2)
        else
            love.graphics.draw(self.image, self.stay, x, y, 0, -2, 2, 12, 0)
        end
    end,
}

return character3