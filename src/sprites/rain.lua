local class = require "hump.class"

local rain = class {
    TotalParticles = 640,
    
    init = function(self)
        self.particles = {}

        for i = 1, self.TotalParticles do
            self.particles[i] = {
                type = love.math.random(2),
                x = love.math.random(-64, 384),
                y = love.math.random(240)
            }
        end
    end,

    update = function(self, dt, inc)
        for i, particle in ipairs(self.particles) do
            particle.y = particle.y + dt * 256 * particle.type
            particle.x = particle.x - inc * 64 * dt

            if particle.y >= 240 then
                particle.y = 0
                particle.x = love.math.random(-64, 384)
                particle.type = love.math.random(2)
            end
        end
    end,

    draw = function(self)
        love.graphics.setColor(0.2, 0.4, 0.7)
        for i, particle in ipairs(self.particles) do
            love.graphics.setPointSize(3 - particle.type)
            love.graphics.points(particle.x, particle.y)
        end
        love.graphics.setPointSize(1)
        love.graphics.setColor(1, 1, 1)
    end
}

return rain