local class = require "hump.class"
local anim8 = require "anim8"
local input = require "inputs"

local character = class {
    init = function(self)
        self.images = {
            love.graphics.newImage("assets/sprites/character.png"),
            love.graphics.newImage("assets/sprites/character-shop.png")
        }
        self.type = 1

        local grid = anim8.newGrid(13, 24, self.images[1]:getWidth(), self.images[1]:getHeight())
        self.walk = anim8.newAnimation(grid("1-2", 1, 1, 1, 3, 1), 0.15)
    end,

    update = function(self, dt, inc, pos)
        if inc ~= 0 and pos > 435 and pos < 2025 then
            self.walk:update(dt)

            if self.walk.status == "paused" then
                self.walk:resume()
            end

            if (inc < 0 and self.walk.flippedH) or
               (inc > 0 and not self.walk.flippedH) then
                self.walk:flipH()
            end
        else
            self.walk:pauseAtStart()
        end
    end,

    draw = function(self, pos)
        local offset = 0
        if pos > 1888 then
            offset = pos - 1888
        elseif pos < 595 then
            offset = pos - 595
        end

        self.walk:draw(self.images[self.type], 153 + offset, 170, 0, 2, 2)
    end
}

return character