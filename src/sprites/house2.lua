local class = require "hump.class"

local house2 = class {
    init = function(self)
        self.image = love.graphics.newImage("assets/sprites/house2.png")
        self.indoor = love.graphics.newImage("assets/sprites/indoor-house2.png")
    end,

    draw = function(self, pos)
        local currentPos = math.min(pos, 1888)

        love.graphics.draw(self.indoor, math.max(math.min(578.5 - math.min(pos / 2, 944), 1125 - currentPos), 1017 - currentPos), 105)
        love.graphics.draw(self.image, 1001 - currentPos)
    end
}

return house2