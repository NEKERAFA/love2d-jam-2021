local class = require "hump.class"

local character = class {
    init = function(self)
        self.pos = love.math.random(-48, 2168)
        
        self.inc = (love.math.random(2) == 2 and 1) or -1
        
        self.offset = { 
            value = 0, 
            inc = love.math.random() * 2 - 1
        }
        
        self.image = love.graphics.newImage("assets/sprites/car.png")
        
        self.sprites = {
            bg = love.graphics.newQuad(0, 0, 48, 24, self.image:getWidth(), self.image:getHeight()),
            fg = love.graphics.newQuad(48, 0, 48, 24, self.image:getWidth(), self.image:getHeight())
        }
        
        self.colour = { love.math.random(0, 255), love.math.random(0, 255), love.math.random(0, 255) }
    end,

    update = function(self, dt)
        self.pos = self.pos + 256 * self.inc * dt

        if self.pos < -48 then
            self.inc = (love.math.random(2) == 2 and 1) or -1
            
            if self.inc < 0 then self.pos = 2168 end
        elseif self.pos > 2168 then
            self.inc = (love.math.random(2) == 2 and 1) or -1

            if self.inc > 0 then self.pos = 0 end
        end

        self.offset.value = self.offset.value + self.offset.inc * dt * 8
        if self.offset.value > 1 or self.offset.value < -1 then
            self.offset.inc = -self.offset.inc
        end
    end,
    
    draw = function(self, pos)
        local sx = self.inc * 2
        local ox = (self.inc > 0 and 0) or 48

        love.graphics.setColor(self.colour)
        love.graphics.draw(self.image, self.sprites.bg, self.pos - pos, 208 + self.offset.inc, 0, sx, 2, ox, 0)
        love.graphics.setColor(1, 1, 1)
        love.graphics.draw(self.image, self.sprites.fg, self.pos - pos, 208 + self.offset.inc, 0, sx, 2, ox, 0)
    end
}

return character