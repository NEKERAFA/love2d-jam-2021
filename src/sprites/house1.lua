local class = require "hump.class"

local house1 = class {
    init = function(self)
        self.image = love.graphics.newImage("assets/sprites/house.png")
    end,

    draw = function(self, pos)
        love.graphics.draw(self.image, 1708 - math.min(pos, 1888))
    end
}

return house1