local class = require "hump.class"

local background = class {
    init = function(self)
        self.image = love.graphics.newImage("assets/sprites/background.png")
    end,

    draw = function(self, pos)
        love.graphics.draw(self.image, 664 - math.min(pos / 2, 944), 55, 0, 2, 1)
    end
}

return background