local baton = require "baton"

local inputs = baton.new {
    controls = {
        left = { 'key:left', 'key:a' },
        right = { 'key:right', 'key:d' },
        up = { 'key:up', 'key:w' },
        down = { 'key:down', 'key:s' },
        enter = { 'key:return' }
    }
}

return inputs