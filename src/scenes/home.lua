local input = require "inputs"
local character3 = require "sprites.character3"
local keycap = require "ui.keycap"
local gamestate = require "hump.gamestate"

local home = {
    init = function(self)
        self.bg = love.graphics.newImage("assets/sprites/room.png")
        self.character = {
            image = love.graphics.newImage("assets/sprites/character-room.png"),
            type = 1,
            sprites = {}
        }
        self.character.sprites[1] = love.graphics.newQuad(0, 0, 32, 44, self.character.image:getWidth(), self.character.image:getHeight())
        self.character.sprites[2] = love.graphics.newQuad(32, 0, 32, 44, self.character.image:getWidth(), self.character.image:getHeight())
        
        self.scene = {
            type = 0,
            clock = 0
        }

        self.character3 = character3()
        self.pos = { x = 23, y = 153 }

        self.keycap = keycap()

        self.lastNumbers = {}
    end,

    enter = function(self, to, anxiety)
        self.anxiety = anxiety
    end,

    update = function(self, dt)
        if self.scene.type == 0 then
            self.keycap:update(dt)

            local toRemove = {}
            for i, number in ipairs(self.lastNumbers) do
                number.clock = number.clock + dt
                if number.clock >= 1 then
                    table.insert(toRemove, i)
                end
            end
            for i, pos in ipairs(toRemove) do
                table.remove(self.lastNumbers, pos)
            end

            if input:pressed("enter") then
                local number = { 
                    value = love.math.random(1, 5),
                    clock = 0
                }
                table.insert(self.lastNumbers, number)

                self.anxiety = math.max(self.anxiety - number.value, 0)
            end

            if self.anxiety == 0 then
                self.scene.type = 1
            end
        elseif self.scene.type == 1 then
            self.scene.clock = self.scene.clock + dt            

            if self.scene.clock >= 2 then
                self.scene.type = 2
                self.scene.clock = 0 
            end
        elseif self.scene.type == 2 then
            self.scene.clock = self.scene.clock + dt            

            if self.scene.clock >= 0.5 then
                self.anxiety = self.anxiety + 10
                self.scene.clock = 0
            end

            if self.anxiety == 100 then
                self.scene.type = 3
                self.scene.clock = 0
            end
        elseif self.scene.type == 3 then
            self.scene.clock = self.scene.clock + dt

            if self.scene.clock >= 1 then
                self.scene.type = 4
                self.scene.clock = 0
                self.character.type = 2
            end
        elseif self.scene.type == 4 then
            self.character3:update(dt)

            self.pos.x = math.min(self.pos.x + dt * 32, 195)
            self.pos.y = 153 - (23 - self.pos.x) / 172 * 22

            if self.pos.x == 195 then
                self.character3.type = 2
                self.scene.type = 5
                self.pos.x = 195
                self.pos.y = 175
            end
        elseif self.scene.type == 5 then
            self.scene.clock = self.scene.clock + dt
            
            if self.scene.clock >= 8 then
                gamestate.pop()
            end
        end
    end,

    draw = function(self)
        love.graphics.draw(self.bg)

        if self.scene.type == 0 or self.scene.type == 2 then
            love.graphics.setColor(1, 0, 0, 1)
            love.graphics.rectangle("fill", 10, 10, self.anxiety, 10)
            love.graphics.setColor(1, 1, 1, 1)
            love.graphics.rectangle("line", 10, 10, 100, 10)
            love.graphics.print("anxiety", 10, 22)
        end

        if self.scene.type == 0 then
            for i, number in ipairs(self.lastNumbers) do
                love.graphics.setColor(1, 1, 1, number.clock)
                love.graphics.print("+" .. tostring(number.value) .. " chocolate", 10, 22 + 40 * number.clock)
            end
            love.graphics.setColor(1, 1, 1, 1)

            self.keycap:draw()
        end

        if self.scene.type == 5 then
            love.graphics.print("Don't worry. It's okay. I will help you")
        end

        love.graphics.draw(self.character.image, self.character.sprites[self.character.type], 206, 169)
    
        if self.scene.type > 3 then
            self.character3:draw(self.pos.x, self.pos.y)
        end
    end
}

return home