local input = require "inputs"
local tween = require "tween"
local gamestate = require "hump.gamestate"
local room = require "scenes.home"
local floor = require "sprites.floor"
local house1 = require "sprites.house1"
local house2 = require "sprites.house2"
local house3 = require "sprites.house3"
local background = require "sprites.background"
local character = require "sprites.character"
local tree = require "sprites.tree"
local rain = require "sprites.rain"
local car = require "sprites.car"
local arrow = require "ui.arrow"
local keycap = require "ui.keycap"

local street = {
    init = function(self)
        self.floor = floor()

        self.background = background()
        
        self.house1 = house1()
        self.house2 = house2()
        self.house3 = house3()

        self.character = character()
        
        self.trees = {
            tree(2000),
            tree(1800),
            tree(1600),
            tree(1300),
            tree(1100),
            tree(800),
            tree(600)
        }

        self.cars = {}
        for i = 1, 10 do
            self.cars[i] = car()
        end

        self.rain = rain()

        self.arrow = arrow()
        self.keycap = keycap()
        
        self.position = 1888
        
        self.acceleration = {
            value = 0,
            state = "staying"
        }

        self.acceleration.accelerate = tween.new(0.1, self.acceleration, { value = 1 }, 'inQuad')

        self.filter = love.graphics.newImage("assets/sprites/filter.png")
        
        self.anxiety = {
            value = 10,
            clock = 0
        }
    end,

    update = function(self, dt)
        if self.acceleration.state == "starting" then
            local complete = self.acceleration.accelerate:update(dt)

            if complete then
                self.acceleration.state = "moving"
            end
        end
        
        local inc = - input:get("left") + input:get("right")

        if inc ~= 0 and self.acceleration.state == "staying" then
            self.acceleration.value = self.acceleration.value
            self.acceleration.state = "starting"
            self.acceleration.accelerate:reset()
        end

        self.position = math.max(math.min(self.position + self.acceleration.value * inc * dt * 96, 2025), 435)
        self.character:update(dt, inc, self.position)

        self.rain:update(dt, inc)

        if self.character.type == 1 then
            if self.position >= 959 and self.position <= 1023 then
                if input:pressed('enter') then
                    self.character.type = 2
                end

                self.keycap:update(dt)
            elseif self.position >= 1168 then
                self.arrow:update(dt)
            end
        elseif self.character.type == 2 then
            if self.position >= 610 and self.position <= 666 then
                if input:pressed('enter') then
                    gamestate.switch(room, self.anxiety.value)
                end

                self.keycap:update(dt)
            elseif self.position >= 891 then
                self.arrow:update(dt)
            end
        end

        for i, car in ipairs(self.cars) do
            car:update(dt)
        end

        self.anxiety.clock = self.anxiety.clock + dt
        if self.anxiety.clock > 1 then
            self.anxiety.clock = 1 - self.anxiety.clock
            self.anxiety.value = math.min(self.anxiety.value + love.math.random(5, 10), 100)
        end
    end,

    draw = function(self)
        love.graphics.clear(0.5, 0.5, 0.5)

        local pos = math.max(self.position, 595)

        self.background:draw(pos)

        self.house1:draw(pos)
        self.house2:draw(pos)
        self.house3:draw(pos)

        self.floor:draw(pos)
        
        self.character:draw(self.position)

        love.graphics.setColor(1, 0, 0, 1)
        love.graphics.rectangle("fill", 10, 10, self.anxiety.value, 10)
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.rectangle("line", 10, 10, 100, 10)
        love.graphics.print("anxiety", 10, 22)

        self.rain:draw()

        for i, tree in ipairs(self.trees) do
            tree:draw(pos)
        end

        for i, car in ipairs(self.cars) do
            car:draw(pos)
        end

        if self.character.type == 1 then
            if self.position >= 959 and self.position <= 1023 then
                self.keycap:draw()
            elseif self.position >= 1168 then
                self.arrow:draw()
            end
        elseif self.character.type == 2 then
            if self.position >= 610 and self.position <= 666 then
                self.keycap:draw()
            elseif self.position >= 891 then
                self.arrow:draw()
            end
        end

        love.graphics.draw(self.filter)
    end
}

return street
