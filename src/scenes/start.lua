local input = require "inputs"
local gamestate = require "hump.gamestate"
local street = require "scenes.street"

local start = {
    init = function(self)
        self.font = love.graphics.newFont("assets/fonts/PixelOperator8.ttf", 16)
    end,

    update = function (self, dt)
        if input:pressed("enter") then
            gamestate.push(street)
        end
    end,

    draw = function (self)
        local defaultFont = love.graphics.getFont()
        love.graphics.setFont(self.font)
        love.graphics.printf("~ Another eating disorder ~", 10, 50, 300, "center")
        love.graphics.setFont(defaultFont)
        love.graphics.printf("press [enter] to start", 10, 200, 300, "center")
    end
}

return start