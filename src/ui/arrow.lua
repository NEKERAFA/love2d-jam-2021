local class = require "hump.class"
local tween = require "tween"

local arrow = class {
    init = function(self)
        self.image = love.graphics.newImage("assets/sprites/arrow.png")
        self.rotation = - math.pi / 16
        self.anim = tween.new(2, self, { rotation = math.pi / 16 }, 'inOutQuad')
        self.anim:set(1)
        self.inc = 1
    end,

    update = function(self, dt)
        self.anim:set(self.anim.clock + self.inc * dt)

        if self.anim.clock == 2 or self.anim.clock == 0 then
            self.inc = -self.inc
        end
    end,
    
    draw = function(self)
        love.graphics.draw(self.image, 16, 104, self.rotation, 1, 1, 8, 16)
    end
}

return arrow