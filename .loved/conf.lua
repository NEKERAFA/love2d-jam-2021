{
  love = {
    on_system = true,
    version = "LOVE 11.3 (Mysterious Mysteries)"
  },
  scm = {
    on_system = true,
    uses = "git"
  },
  version = "1.0-alpha"
}