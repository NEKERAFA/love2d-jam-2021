local push = require "push"
local gamestate = require "hump.gamestate"
local start = require "scenes.start"
local input = require "inputs"

function love.load()
    love.graphics.setDefaultFilter("nearest", "nearest")

    local gameWidth, gameHeight = 320, 240
    local screenConfig = {
        fullscreen = false,
        canvas = true,
        pixelperfect = true,
        highdpi = false
    }

    push:setupScreen(gameWidth, gameHeight, love.graphics.getWidth(), love.graphics.getHeight(), screenConfig)

    local defaultFont = love.graphics.newFont("assets/fonts/PixelOperator8.ttf", 8)

    love.graphics.setFont(defaultFont)

    gamestate.switch(start)
end

function love.update(dt)
    input:update()
    gamestate.update(dt)
end

function love.draw()
    push:start()

    gamestate.draw()
    
    push:finish()
end
